---
slug: gnome-stack-for-dotnet
title: Gnome Stack for .NET
authors: [btg]
tags: [gnome, stack]
---

GnomeStack for .NET is a set of libraries that extend the BCL and the
different UI stacks for .NET, and extends common libraries with a focus
Developer Experience and automation.

With .NET's evolving feature sets into trimming, AOT, `Span<T>`, hardware
instrinsics, Source Code generators, it opens doors to be build interesting
tools that have recently been created with Rust or Go.
